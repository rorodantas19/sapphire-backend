/* Securely compares 64 bytes. Should be resistant to timing attacks. TODO:
 * Check */

#define ROUND(idx) success &= (a[idx] == b[idx]);

static __attribute__((optimize("O0"))) gboolean
secure_compare_64(const char *a, const char *b)
{
	gboolean success = TRUE;

	ROUND(0);
	ROUND(1);
	ROUND(2);
	ROUND(3);
	ROUND(4);
	ROUND(5);
	ROUND(6);
	ROUND(7);
	ROUND(8);
	ROUND(9);
	ROUND(10);
	ROUND(11);
	ROUND(12);
	ROUND(13);
	ROUND(14);
	ROUND(15);
	ROUND(16);
	ROUND(17);
	ROUND(18);
	ROUND(19);
	ROUND(20);
	ROUND(21);
	ROUND(22);
	ROUND(23);
	ROUND(24);
	ROUND(25);
	ROUND(26);
	ROUND(27);
	ROUND(28);
	ROUND(29);
	ROUND(30);
	ROUND(31);
	ROUND(32);
	ROUND(33);
	ROUND(34);
	ROUND(35);
	ROUND(36);
	ROUND(37);
	ROUND(38);
	ROUND(39);
	ROUND(40);
	ROUND(41);
	ROUND(42);
	ROUND(43);
	ROUND(44);
	ROUND(45);
	ROUND(46);
	ROUND(47);
	ROUND(48);
	ROUND(49);
	ROUND(50);
	ROUND(51);
	ROUND(52);
	ROUND(53);
	ROUND(54);
	ROUND(55);
	ROUND(56);
	ROUND(57);
	ROUND(58);
	ROUND(59);
	ROUND(60);
	ROUND(61);
	ROUND(62);
	ROUND(63);

	return success;
}

#undef ROUND
