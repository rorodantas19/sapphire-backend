# Run sapphire-backend (installed globally to /usr/local/bin) in a jail
firejail --private=accounts/$1 sapphire-backend --jailed
